<form class="d-flex flex-column gap-3" action="{{$action}}"
    method=@if ($method === App\View\Components\dynamic\Form::REQUEST_METHOD_GET) "{{ App\View\Components\dynamic\Form::REQUEST_METHOD_GET }}" @else "{{ App\View\Components\dynamic\Form::REQUEST_METHOD_POST }}" @endif>
    @csrf
    @method($method)

    @foreach ($config['form']['fields'] as $field)
        @if ($field['type'] === App\View\Components\dynamic\Form::FIELD_TYPE_CUSTOM)
            {{ ${$field['name']} }}
        @else
            @php
                $component = 'dynamic.fields.' . strtolower($field['type']);
            @endphp
            @isset($record)
                <x-dynamic-component :component="$component" :field="$field" :record="$record" />
            @else
                <x-dynamic-component :component="$component" :field="$field" />
            @endisset
        @endif
    @endforeach

    {{$slot}}

    <button class="btn btn-primary align-self-start" type="submit"><i class="bi bi-floppy-fill me-2"></i>{{  __('helpers.save') }}</button>
</form>
