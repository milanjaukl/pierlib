
  <div class="mb-3">
    <label for="{{$field['id']}}" class="form-label">{{$field['label']}}</label>
    <textarea class="form-control" id="{{$field['id']}}" rows="3"></textarea>
  </div>