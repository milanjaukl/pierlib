<div>
    <div>
        <label for="{{$field['id']}}" class="form-label">{{$field['label']}}</label>
        <input name="{{$field['name']}}" type="text" class="form-control @error($field['name']) is-invalid @enderror" id="{{$field['id']}}"}}" @isset($record)
        value="{{$record[$field['name']]}}"          
        @endisset>
        @error($field['name'])
        <div class="invalid-feedback">
          {{$message}}
        </div>
        @enderror
      </div>
</div>