<div>
    <div>
        <label for="exampleInputEmail1" class="form-label">{{$field['label']}}</label>
        <select name="{{$field['name']}}" class="form-select @error($field['name']) is-invalid @enderror" aria-label="Default select example">
            <option value="" disabled selected>{{ __('helpers.select') }}</option>
            @foreach ($field['select']['model']::all() as $item)
                <option 
                    value="{{$item[$field['select']['value']]}}"
                    @if (isset($record) && $item[$field['select']['value']] == $record[$field['name']]) 
                        selected 
                        class="bg-warning"
                    @endif
                >
                    {{$item[$field['select']['text']]}}
                </option>
            @endforeach
        </select>
        @error($field['name'])
            <div class="invalid-feedback">
                {{$message}}
            </div>
        @enderror
    </div>
</div>