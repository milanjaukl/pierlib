{{-- {{dd($config)}} --}}
<table class="table table-striped border border-1">
    <thead>
      <tr>
         @foreach ($config['columns'] as $attribute => $value)
        <th scope="col">{{$value['header']}}</th>
        @endforeach
        @foreach ($config['actions'] as $value)
        <th scope="col"></th>
        @endforeach
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $item)
      {{-- {{dd($item)}} --}}
      <tr>
        @foreach ($config['columns'] as $attribute => $value)
        @if (isset($value['alias']))
          <td>{{ getAliasValue($item, ['criticality', 'name']) }}</td>
        @else
        <td>{{ $item[$value['data']] }}</td>

        @endif
        @endforeach

        @foreach ($config['actions'] as $action)
        

        @if($action['type'] === 'form')
        <form method="{{{($action['formConfig']['method'] === 'get') ? 'GET' : 'POST'}}}" action="{{route($action['formConfig']['route'], ['id' => $item->id])}}">
          @csrf
          @method($action['formConfig']['method'])
          @foreach ($action['formConfig']['inputs'] as $input)
              <{{$input['type']}} class="{{$input['class']}} id="{{$input['id']}}">{{$input['text']}} </{{$input['type']}}>
          @endforeach
        </form>
        @elseif ($action['type'] === 'html')
        @if(isset($action['placeholders']))
          <td class="text-center">
            {!! replacePlaceholders($item, $action['html'], $action['placeholders'])  !!}
          </td>
        @else
        <th id="html">{!! $action['html'] !!}</th>
        @endif
        @else
        <th scope="col">
          <{{$action['type']}} class="{{$action['class']}}" href="{{route($action['route'], ['id' => $item->id])}}">{{$action['text']}}</{{$action['type']}}>
        </th>
        @endif
      

        @endforeach
      </tr>
      @endforeach
      
    </tbody>
  </table>