<div class="d-flex flex-column">
    @if($config['type'] !== 'hidden')
    <label for="{{$config['id']}}" class="form-label">{{$config['label']}}</label>
    @endif

    @if ($config['type'] === 'textarea')
        <textarea @isset($config['x-model']) x-model="{{$config['x-model'] }}" @endisset id="{{$config['id']}}" name="{{$config['name']}}" type="{{$config['type']}}" class="form-control @error ($config['name']) is-invalid @enderror" cols="{{$config['cols']}}" rows="{{$config['rows']}}">{{ $model->{$config['name']} }}</textarea>
    @elseif ($config['type'] === 'select')
    <select id="{{$config['id']}}" name="{{$config['name']}}" class="form-select">
        {{-- {{ ${$config['name']} }} --}}
        @isset (${$config['name']})
            {{ ${$config['name']} }}
        @else
        @foreach ($config['select']['options']::all() as $option)
            <option value="{{ $option->{($config['select']['value'])} }}">{{ $option->{($config['select']['param'])} }}</option>
        @endforeach 
        @endisset
    </select>
    
    @push('scripts')
    <script type="module">
        let id = @json($config['id']);
        $('#'+id).select2({
            width: '20rem',
            placeholder: 'Select'
        });
    </script>
    @endpush

    @else
    <input @isset($config['x-model']) x-model="{{$config['x-model'] }}" @endisset id="{{$config['id']}}" name="{{$config['name']}}" type="{{$config['type']}}" class="form-control @error ($config['name']) is-invalid @enderror " value="{{ $model->{$config['name']} }}">

        @isset (${$config['name']})
            {{ ${$config['name']} }}
        @endisset

    @endif

    @error ($config['name'])
        <div class="invalid-feedback">
            {{$message}}
        </div>
    @enderror
</div>