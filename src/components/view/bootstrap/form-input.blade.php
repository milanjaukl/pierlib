<div class="mb-3">
    <label for="{{ $id }}">{{ $label }}</label>
    @if (empty($type))
        {{ $slot }}
    @elseif ($type === 'textarea')
        <textarea name="{{ $name }}" id="{{ $id }}" class="form-control" cols="{{ $cols }}"
            rows="{{ $rows }}" {{ isset($required) && $required ? 'required' : '' }}> </textarea>
    @elseif($type === 'checkbox')
        <input type="checkbox" name="{{ $name }}" id="{{ $id }}" class="form-check">
    @else
        <input
            {{ $attributes->merge(['class' => 'form-control', 'type' => $type ?: 'text', 'required' => isset($required) && $required ? 'required' : '']) }}
            name="{{ $name }}" id="{{ $id }}">
    @endif
    @if (!empty($type))
    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
    @endif
</div>
