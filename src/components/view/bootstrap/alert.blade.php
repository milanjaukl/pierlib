@if ($type === 'error')
<div class="alert-flash-message alert alert-danger position-fixed bottom-0 end-0 z-3 d-flex gap-2 alert-dismissible fade show me-2 fade-out" role="alert">
    <i class="bi bi-exclamation-triangle-fill"></i>
    <div>
        {{$slot}}
    </div>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@else
<div class="alert-flash-message alert alert-success position-fixed bottom-0 end-0 z-3 d-flex gap-2 alert-dismissible fade show me-2 fade-out" role="alert">
    <i class="bi bi-check2"></i>
    <div>
        {{$slot}}
    </div>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

