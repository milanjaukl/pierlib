<!-- Modal {{$id}} -->
<div class="modal fade" id="{{$id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="margin-top: -10rem">
    <div class="modal-content">
        <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{$heading}}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            {{$slot}}
        </div>
        <div class="modal-footer">
        <button onclick="dispatchModalEvent('storno', '{{$id}}')"  id="{{$id}}-storno" type="button" class="btn btn-secondary" data-bs-dismiss="modal">Storno</button>
        <button onclick="dispatchModalEvent('confirm', '{{$id}}')"  id="{{$id}}-confirm" type="button" class="btn btn-primary">{!! $confirmButtontext ?? 'Proveď'!!}</button>
        </div>
    </div>
    </div>
</div>

@push('scripts')
    <script>
        function dispatchModalEvent(action, modalId) {
            // Create a custom event with details about the action and modal ID
            const event = new CustomEvent(`modal:${action}`, {
                detail: { modalId}
            });
            window.dispatchEvent(event);
        }
    </script>
@endpush