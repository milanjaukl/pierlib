<?php

namespace App\View\Components\Dynamic\Fields;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Number extends Component
{
    /**
     * Create a new component instance.
     */

    public $field;
    public $record;

    public function __construct($field, $record = null)
    {
        $this->field = $field;
        $this->record = $record;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.dynamic.fields.number');
    }
}
