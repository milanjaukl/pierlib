<?php

namespace App\View\Components\dynamic;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class FormInput extends Component
{
    /**
     * Create a new component instance.
     */
    public $id;

    public $name;
    public $label;
    public $type;
    public $value;
    public $rows;
    public $cols;
    public $config;
    public $model;

    public function __construct($config, $model)
    {
        $this->config = $config;
        $this->model = $model;
    }

    // public function __construct($id = '', $name, $label, $type, $value, $cols= '', $rows = '')
    // {
    //     $this->name = $name;
    //     $this->label = $label;
    //     $this->id = $id;
    //     $this->type = $type;
    //     $this->value = $value;
    //     $this->cols = $cols;
    //     $this->rows = $rows;
    // }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.dynamic.form-input',[
            'model' => $this->model,
            'config' => $this->config
        ]);
    }
}
