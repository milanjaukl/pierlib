<?php

namespace App\View\Components\dynamic;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Form extends Component
{
    /**
     * Create a new component instance.
     */

    public $config;
    public $record;
    public $method;
    public $action;

    const FIELD_TYPE_SELECT = 'Select', FIELD_TYPE_TEXT = 'text', FIELD_TYPE_TEXTAREA = 'textarea', FIELD_TYPE_CUSTOM = 'custom', FIELD_TYPE_CHECKBOX = 'checkbox', FIELD_TYPE_NUMBER = 'NUMBER';
    const REQUEST_METHOD_POST = 'POST', REQUEST_METHOD_PUT = 'PUT', REQUEST_METHOD_PATCH = 'PATCH', REQUEST_METHOD_DELETE = 'DELETE', REQUEST_METHOD_GET = 'GET';

    public function __construct($config, $record = null, $action, $method)
    {

        $configPath = base_path("app/Dynamic/Forms/{$config}.php");

        if (!file_exists($configPath)) {
            throw new \Exception("The config file {$config}.php does not exist.");
        }

        $this->config = include $configPath;
        $this->record = $record;
        $this->action = $action;
        $this->method = $method;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.dynamic.form');
    }
}
