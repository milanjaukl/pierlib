<?php

namespace App\View\Components\dynamic;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class Table extends Component
{
    /**
     * Create a new component instance.
     */

    public $config;
    public $id;
    public $data;
    public $actions;

    public function __construct($config)
    {
        $this->id = $config;
        $path = base_path("app/Dynamic/Tables/{$this->id}.php");

        if (File::exists($path)) {
            $this->config = include($path);

            if (isset($this->config['model'])) {
                $modelClass = $this->config['model'];

                if (isset($this->config['filter'])) {
                    $query = $modelClass::query();
                    Log::debug($this->config['filter']);
                    foreach ($this->config['filter'] as $field => $value) {
                        $query->where($field, $value);
                    }
                    $this->data = $query->get();
                } else {
                    $this->data = $modelClass::all();
                }

                if ($this->config['actions']) {
                    $this->actions = $this->config['actions'];
                }
            } else {
                $this->data = [];
            }
        } else {
            $this->data = [];
            dd('nenalezen file');
        }
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.dynamic.table', [
            'config' => $this->config,
            'data' => $this->data,
            'actions' => $this->actions
        ]);
    }
}
