<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite(['resources/scss/main.scss', 'resources/js/app.js', 'resources/js/alpine.js', 'resources/js/documentation/__alpine.js'])
    <title>{{ $title ?? 'PG-BESTARS' }}</title>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    {{-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="position-relative d-flex flex-column">
    <header>
        <x-navbar>
        </x-navbar>
    </header>

    <main class="flex-grow-1" style="min-height: 1000px">
        <main class="d-flex flex-column gap-2">
            <div class="mx-auto" style="width: 1200px">
                @isset($breadcrumb)
                <div class="mx-auto mt-3">
                    {{$breadcrumb}}

                </div>
                @endisset

                @isset($title)
                    <div class="mx-auto mt-1">
                        <h3 class="text-decoration-underline">{{$title}}</h3>
                    </div>
                @endisset
            </div>

            <div class="mx-auto" style="width: 1200px;">
                {{ $content ?? 'CONTEND DOES NOT SET' }}
            </div>
        </main>
    </main>

    <x-footer>
    </x-footer>

    @if($errors->any())
        <x-bootstrap.alert type="error">{{$errors->all()[0]}}</x-bootstrap.alert>
    @endif

    @if(session('success'))
        <x-bootstrap.alert type="alert">{{ session('success') }}</x-bootstrap.alert>
    @endif

    <i id="scrollBtn" class="bi bi-arrow-up-circle fs-1 shadow-lg position-fixed rounded-pill text-center  bottom-0 end-0 me-4 mb-4 cursor-pointer border border-1 d-none" style="width: 60px; height: 60px; background-color: #EAEEF2"></i>
    <script type="module">
        

        $(document).ready(function() {
            fadeOut();
            let mybutton = document.getElementById("scrollBtn");
            // Get all elements with the class 'fade-out'
            
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {scrollFunction()};
                function scrollFunction() {
                if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
                    mybutton.classList.remove('d-none')
                } else {
                    mybutton.classList.add('d-none')

                }
            }

            window.triggerErrorEvent = function(type, message) {
                // console.log('event vytvoren')
                const errorEvent = new CustomEvent(`alert:${type}`, {
                    detail: {message}
                })
                window.dispatchEvent(errorEvent);
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }

            

            mybutton.addEventListener('click', (event) => {
                topFunction()
            })

            window.addEventListener('alert:error', function (event)  {
                console.log('alert:error rozpoznan')
                let htmlContent = `<div class='alert-flash-message alert alert-danger position-fixed bottom-0 end-0 z-3 d-flex gap-2 alert-dismissible fade show me-2 fade-out' role='alert'>
                                    <i class="bi bi-exclamation-triangle-fill"></i>

                                    <div>
                                        ${event.detail.message}
                                    </div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>`

                document.body.insertAdjacentHTML('afterbegin', htmlContent);
                fadeOut()
            })

            window.addEventListener('alert:success', function (event)  {
                console.log('alert:error rozpoznan')
                let htmlContent = `<div class='alert-flash-message alert alert-success position-fixed bottom-0 end-0 z-3 d-flex gap-2 alert-dismissible fade show me-2 fade-out' role='alert'>
                                    <i class="bi bi-check2"></i>
                                    <div>
                                        ${event.detail.message}
                                    </div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>`

                document.body.insertAdjacentHTML('afterbegin', htmlContent);
                fadeOut()
            })

            function fadeOut () {
                var fadeOutElements = document.getElementsByClassName('fade-out');

                // Apply the fade-out effect to all elements with the class 'fade-out'
                Array.from(fadeOutElements).forEach(function(element) {
                    fadeOutEffect(element);
                });
            }

            function fadeOutEffect(element) {
                setTimeout(function() {
                    var fadeEffect = setInterval(function () {
                    if (!element.style.opacity) {
                        element.style.opacity = 1;
                    }
                    if (element.style.opacity > 0) {
                        element.style.opacity -= 0.1;
                    } else {
                        clearInterval(fadeEffect);
                        element.style.display = 'none';
                    }
                    }, 50); // Adjust this value to control the speed of the fade-out
                }, 1000); // Delay in milliseconds
            }
        });
    </script>
    @stack('scripts')
</body>
</html>
