<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateFormConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:form-config {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new form configuration template.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
        $filePath = base_path("app/Dynamic/Forms/{$name}.php");

        if (file_exists($filePath)) {
            $this->error("The file {$name}.php already exists!");
            return;
        }

        $template = <<<PHP
    <?php
    use App\View\Components\dynamic\Form;


    return [
        'form' => [
            'action' => 'get.home.index',
            'method' => 'GET',
            'fields' => [
                // EXAMPLE
                [
                    'type' => Form::FIELD_TYPE_TEXT,
                    'name' => User::COL_NAME,
                    'label' => 'Name',
                    'rules' => 'required',
                ],
                // More fields...
            ],
            'submitLabel' => 'Submit',
        ],
    ];
    PHP;

        file_put_contents($filePath, $template);
        $this->info("Form config template created at {$filePath}");
    }
}
