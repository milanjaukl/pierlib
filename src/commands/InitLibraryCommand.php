<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;

class InitLibraryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:library';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes the library by creating symlinks';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Define the paths
        $libraryShort = 'vendor\pierburg\dynamic\src';
        
        $dynamicTarget = base_path($libraryShort.'\components\app\dynamic');
        // $bootstrapTarget = base_path($libraryShort.'\components\app\bootstrap');

        $dynamicTargetComponent = base_path($libraryShort.'\components\view\dynamic');
        $bootstrapTargetComponent = base_path($libraryShort.'\components\view\bootstrap');

        
        $componentsPath = base_path('app\View\Components');
        $componentsPathView = base_path('resources\views\components');
        $dynamicConfigPath = base_path('app\Dynamic');

        // Check if the View\Components directory exists, if not, create it
        if (!is_dir($componentsPath)) {
            mkdir($componentsPath, 0777, true);
            $this->info("Created directory: $componentsPath");
        }

        // // Change directory (not necessary for creating symbolic links, but added for completeness)
        chdir($componentsPath);

        // // Create symbolic links
        exec("mklink /D dynamic \"$dynamicTarget\"");
        // exec("mklink /D bootstrap \"$bootstrapTarget\"");

        chdir($componentsPathView);

        exec("mklink /D dynamic \"$dynamicTargetComponent\"");
        exec("mklink /D bootstrap \"$bootstrapTargetComponent\"");

        chdir(base_path());

        if (!is_dir($dynamicConfigPath)) {
            mkdir($dynamicConfigPath, 0777, true);
            File::makeDirectory($dynamicConfigPath.'\Forms', 0755, true);
            File::makeDirectory($dynamicConfigPath.'\Tables', 0755, true);

            $this->info("Created directory: $dynamicConfigPath");
        }

        $this->info('Symbolic links and sctructure created successfully.');
    
    }
}
