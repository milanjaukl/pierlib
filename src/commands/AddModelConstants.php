<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddModelConstants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'model:add-constants {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds static variables to a model based on its database table columns.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $modelName = $this->argument('model');
$modelClass = "App\\Models\\$modelName";

if (!class_exists($modelClass)) {
    $this->error("Model $modelName does not exist.");
    return;
}

$model = new $modelClass;
$tableName = $model->getTable();
$columns = Schema::getColumnListing($tableName);

$constants = collect($columns)->map(function ($column) {
    return '    public const COL_' . strtoupper($column) . " = '$column';";
})->implode("\n");

$modelPath = app_path("Models/{$modelName}.php");
$modelContent = file_get_contents($modelPath);

// Adjusted pattern to match up to and including the opening curly brace of the class definition
$pattern = "/(class\s+$modelName\s+extends\s+[a-zA-Z_\\\]+[\s\S]*?{)/";
// Use the replacement to position the constants right after the opening curly brace
$replacement = "$1\n$constants";

$newModelContent = preg_replace($pattern, $replacement, $modelContent, 1);

// Check if the replacement was successful to avoid overwriting files without changes
if ($newModelContent && $newModelContent !== $modelContent) {
    file_put_contents($modelPath, $newModelContent);
    $this->info("Constants added to $modelName model.");
} else {
    $this->error("Failed to add constants to $modelName model or no changes needed.");
}

    }
}
