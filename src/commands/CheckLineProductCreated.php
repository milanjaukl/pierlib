<?php

namespace App\Console\Commands;

use App\Events\LineProductCreated;
use App\Events\ProductionLineUpdate;
use App\Models\LineProduct;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class CheckLineProductCreated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:check-line-product-created';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        while(true) {
            $lastChecked = Cache::get('last_checked_time', now()->subSeconds(10));
            $newRecords = LineProduct::where('created_at', '>', $lastChecked)->get();

            if ($newRecords->isNotEmpty()) {
                foreach ($newRecords as $record) {
                    $line = \App\Models\Line::find($record->line_id);
                    //get count of product for a line
                    $lineProductCount = LineProduct::where('line_id', $record->line_id)->count();
                    //from a total count of product for a line get the count of product that are ok
                    $lineProductOkCount = LineProduct::where('line_id', $record->line_id)->where('ok', 1)->count();
                    //from a total count of product for a line get the count of product that are no
                    $lineProductNoCount = LineProduct::where('line_id', $record->line_id)->where('ok', 0)->count();
                    $record->kpi  = [
                                        'oee' => 0,
                                        'availability' => 0,
                                        'performance' => 0,
                                        'quality' => 0,
                                        'products' => [
                                            'total' => $lineProductCount,
                                            'ok' => $lineProductOkCount,
                                            'no' => $lineProductNoCount,
                                        ]
                    ];
                    $channel = $this->getLineChannel($record->line_id);
                    event(new ProductionLineUpdate($record, $channel));
                    $this->info('['.now().'] Line product with id '.$record->id.' from line '.$record->line_id.' push notification successfully.');$this->info('Line product with id.'.$record->id.'from line '.$record->line_id.' push notification successfully.');
                }

                $latestRecord = $newRecords->last();
                Cache::put('last_checked_time', $latestRecord->created_at);
            }
            sleep(1);
        }
    }
    private function getLineChannel($lineId)
    {
        return "line.{$lineId}.updates";
    }
}
