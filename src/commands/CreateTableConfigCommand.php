<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateTableConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:table-config {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new table configuration template.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
    $filePath = base_path("app/Dynamic/Tables/{$name}.php");

    if (file_exists($filePath)) {
        $this->error("The file {$name}.php already exists!");
        return false;
    }

    $template = <<<PHP
<?php

use App\Models\ModelName; // Replace ModelName with your actual model

return [
    'id' => '',
    'class' => 'table',
    'model' => ModelName::class,
    'columns' => [
        [
            'header' => 'ID',
            'data' => ModelName::COL_ID, // Replace COL_ID with the actual constant from your model
        ],
        [
            'header' => 'Name',
            'data' => ModelName::COL_NAME, // Replace COL_NAME with the actual constant from your model
        ],
        // Add more columns as needed
    ],
    'actions' => [
        // Define actions (edit, delete, etc.) here
    ],
    'filter' => [
        // Configuration for filters, if needed
    ],
];
PHP;

    file_put_contents($filePath, $template);
    $this->info("Table config template created at {$filePath}");
    }
}
