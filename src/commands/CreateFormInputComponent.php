<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;


class CreateFormInputComponent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:form-input {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Blade component in a specific folder and update its model';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
        $directory = "Dynamic/Fields";
        $fullComponentName = "{$directory}/{$name}";

        // Step 1: Use Artisan to generate the component
        Artisan::call("make:component {$fullComponentName}");

        // Step 2: Alter the newly created component class
        $this->alterComponentClass($name, $directory);

        $this->info("Component {$name} created and altered successfully in app/View/Components/{$directory}.");

        $this->addFieldTypeConstantToForm($name);

        $this->info("Component {$name} created and altered successfully, and FIELD_TYPE_{$name} added to Form component.");
    
    }

    protected function alterComponentClass($name, $directory)
    {
        $className = Str::studly($name);
        $directoryPath = str_replace('/', '\\', $directory); // Adjust directory path for namespace
        $filePath = app_path("View/Components/{$directory}/{$className}.php");

        // Ensure the component class file exists
        if (!file_exists($filePath)) {
            $this->error("The component class file does not exist: {$filePath}");
            return;
        }

        // Read the current content of the file
        $content = file_get_contents($filePath);

        // Define the new constructor and public variable
        $newConstructor = <<<EOT
public \$field;

public function __construct(\$field)
{
    \$this->field = \$field;
}
EOT;

        // Replace the existing constructor with the new one or append if not found
        $pattern = '/public\s+function\s+__construct\(\)\s*\{\s*\}/';
        if (preg_match($pattern, $content)) {
            $alteredContent = preg_replace($pattern, $newConstructor, $content);
        } else {
            // If the constructor doesn't exist, append the new constructor after the class declaration
            $classPattern = "/class\s+$className\s+extends\s+Component\s*\{/";
            $alteredContent = preg_replace($classPattern, "$0\n    " . str_replace("\n", "\n    ", $newConstructor), $content, 1);
        }

        // Write the altered content back to the file
        file_put_contents($filePath, $alteredContent);
        $this->info("Updated component class {$className} with a new constructor.");
    }

    protected function addFieldTypeConstantToForm($name)
{
    $formComponentPath = app_path('View/Components/Dynamic/Form.php');
    if (!file_exists($formComponentPath)) {
        $this->error("The Form component does not exist at: {$formComponentPath}");
        return;
    }

    // Define the new constant
    $constantName = 'FIELD_TYPE_' . strtoupper($name);
    $newConstant = "    public const {$constantName} = '{$name}';\n";

    // Read the current content of the Form component
    $content = file_get_contents($formComponentPath);

    // Insert the new constant after the class opening brace or the last defined constant
    $pattern = '/class\s+Form\s+extends\s+Component\s*{\n?/';
    if (preg_match($pattern, $content)) {
        // Insert the new constant after the first match of class opening
        $alteredContent = preg_replace($pattern, "$0{$newConstant}", $content, 1);
    } else {
        $this->error("Could not locate the class opening brace in Form component.");
        return;
    }

    // Write the altered content back to the Form component file
    file_put_contents($formComponentPath, $alteredContent);
    $this->info("Added constant {$constantName} to Form component.");
}


}
