<?php

namespace App\Console\Commands;

use App\Models\LineProduct;
use Illuminate\Console\Command;
use Faker\Factory as Faker;


class InsertLineProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:insert-line-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $faker  = Faker::create();
        while (true) {
            LineProduct::create([
                'line_id' => rand(1, 3),
                'name' => $faker->word,
                'ok' => $faker->boolean,
            ]);
            $this->info('Line product created successfully.');
            $sleep = rand(1,5);
            sleep($sleep);
        }
        
    }
}
